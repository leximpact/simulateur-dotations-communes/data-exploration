from data_exploration.utils.lecture_data import CRITERES_2021_PATH
from data_exploration.utils.new_load_dgcl_criteres_data import load_dgcl_file

dotations_criteres_2021 = load_dgcl_file(CRITERES_2021_PATH)
