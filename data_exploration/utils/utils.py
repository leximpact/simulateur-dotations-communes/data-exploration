import pandas
from os import listdir, getcwd
from pathlib import Path


# source : 
# https://gitlab.com/incubateur-territoires/startups/dotations-locales/dotations-locales-back/-/blob/14282d87b8b9198f3a4002a56549088af91b7999/dotations_locales_back/common/load_dgcl_criteres_data.py#L85


def load_dgcl_file(path="/data/criteres_repartition_2021.csv"):
    try:
        print(f"Loading {Path(path).resolve()}")
        code_insee = "Informations générales - Code INSEE de la commune"   # valable pour 2020, 2021, 2022, 2024
        data = pandas.read_csv(path, decimal=",", dtype={code_insee: str})
    except FileNotFoundError:
        print("file", path, "was not found")
        print("ls :", os.listdir("."))
        print("cwd :", os.getcwd())
        raise
    return data



def add_of_variable(variable_list, simulation, data_df):
    for variable in variable_list:
        data_df[variable] = simulation.calculate(variable, 2022)
        data_df[variable].fillna(0)
    return data_df


def dsr_bc_score(data_df, code_insee):
    commune = data_df.loc[data_df["code_insee"] == code_insee]
    PFI = 887.451872

    pfi = commune["potentiel_financier_par_habitant"].iloc[0]
    facteur_pot_fin = 1 + ((PFI - pfi) / PFI)
    effort_fiscal = commune["effort_fiscal"].iloc[0]

    if effort_fiscal > 1.2:
        effort_fiscal = 1.2

    if commune["zrr"].iloc[0] == True:
        coeff_zrr = 1.3
    else:
        coeff_zrr = 1

    if commune["population_dgf"].iloc[0] > 10000:
        pop_dgf = 10000
    else:
        pop_dgf = commune["population_dgf"]

    return facteur_pot_fin * pop_dgf * coeff_zrr * effort_fiscal


def calculate_dotations(simulation, year, formated_data):
    dotation_forfaitaire = simulation.calculate("dotation_forfaitaire", year)
    dsu_montant = simulation.calculate("dsu_montant", year)
    dsr_fraction_bourg_centre = simulation.calculate("dsr_fraction_bourg_centre", year)
    dsr_fraction_perequation = simulation.calculate("dsr_fraction_perequation", year)

    dsr_fraction_cible = simulation.calculate("dsr_fraction_cible", year)
    simulated_data = formated_data.copy()
    simulated_data["dotation_forfaitaire"] = dotation_forfaitaire
    simulated_data["dsu_montant"] = dsu_montant
    simulated_data["dsr_fraction_bourg_centre"] = dsr_fraction_bourg_centre
    simulated_data["dsr_fraction_perequation"] = dsr_fraction_perequation
    simulated_data["dsr_fraction_cible"] = dsr_fraction_cible
    simulated_data["dotation_solidarite_rurale"] = (
        simulated_data["dsr_fraction_bourg_centre"]
        + simulated_data["dsr_fraction_perequation"]
        + simulated_data["dsr_fraction_cible"]
    )
    return simulated_data
