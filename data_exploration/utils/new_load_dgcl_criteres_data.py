import pandas
from pandas.api.types import is_string_dtype
from pathlib import Path
import os

# Liste des colonnes qui nous intéressent pour le moment avec leur nom dans openfisca
columns_to_keep_2021 = {
    "Informations générales - Code INSEE de la commune": "code_insee",
    "Informations générales - Nom de la commune": "nom",
    "Dotation forfaitaire - Dotation forfaitaire notifiée N": "dotation_forfaitaire",
    "Dotation de solidarité urbaine - Montant total réparti": "dsu_montant",
    "Dotation de solidarité rurale Bourg-centre - Montant global réparti": "dsr_fraction_bourg_centre",
    "Dotation de solidarité rurale - Péréquation - Montant global réparti (après garantie CN)": "dsr_fraction_perequation",
    "Dotation de solidarité rurale - Cible - Montant global réparti": "dsr_fraction_cible",
    "Informations générales - Population DGF de l'année N": "population_dgf",
    "Potentiel fiscal et financier des communes - Potentiel financier par habitant": "potentiel_financier_par_habitant",
    "Dotation de solidarité rurale - Péréquation - Longueur de voirie en mètres": "longueur_voirie",
    "Dotation de solidarité rurale - Péréquation - Commune située en zone de montagne": "zone_de_montagne",
    "Informations générales - Superficie de l'année N": "superficie",
    "Dotation de solidarité rurale - Péréquation - Population 3 à 16 ans": "population_enfants",
}

columns_to_keep_2022 = {
    "Informations générales - Code INSEE de la commune": "code_insee",
    "Informations générales - Nom de la commune": "nom",
    "Dotation forfaitaire - Dotation forfaitaire notifiée N": "dotation_forfaitaire",
    "Dotation de solidarité urbaine et de cohésion sociale - Montant total réparti": "dsu_montant",
    "Dotation de solidarité rurale - Fraction bourg-centre - Montant global réparti": "dsr_fraction_bourg_centre",
    "Dotation de solidarité rurale - Fraction péréquation - Montant global réparti (après garantie CN)": "dsr_fraction_perequation",
    "Dotation de solidarité rurale - Fraction cible - Montant global réparti": "dsr_fraction_cible",
    "Informations générales - Population DGF de l'année N": "population_dgf",
    "Potentiel fiscal et financier des communes - Potentiel financier par habitant final": "potentiel_financier_par_habitant",
    "Dotation de solidarité rurale - Fraction péréquation - Longueur de voirie en mètres": "longueur_voirie",
    "Dotation de solidarité rurale - Fraction péréquation - Commune située en zone de montagne": "zone_de_montagne",
    "Informations générales - Superficie de l'année N": "superficie",
    "Dotation de solidarité rurale - Fraction péréquation - Population 3 à 16 ans": "population_enfants",
}

# non utilisé au bénéfice du code de leximpact-dotations-back :
# columns_to_keep_2024 = {
#     "Informations générales - Code INSEE de la commune": "code_insee",
#     "Informations générales - Nom de la commune": "nom",
#     "Dotation forfaitaire - Dotation forfaitaire notifiée N": "dotation_forfaitaire",
#     "Dotation de solidarité urbaine et de cohésion sociale - Montant total réparti": "dsu_montant",
#     "Dotation de solidarité rurale - Fraction bourg-centre - Montant global réparti": "dsr_fraction_bourg_centre",
#     "Dotation de solidarité rurale - Fraction péréquation - Montant global réparti": "dsr_fraction_perequation",
#     "Dotation de solidarité rurale - Fraction cible - Montant global réparti": "dsr_fraction_cible",
#     "Informations générales - Population DGF de l'année N": "population_dgf",
#     "Potentiel fiscal et financier des communes - Potentiel financier par habitant final": "potentiel_financier_par_habitant",
#     "Dotation de solidarité rurale - Fraction péréquation - Longueur de voirie en mètres": "longueur_voirie",
#     "Dotation de solidarité rurale - Fraction péréquation - Commune située en zone de montagne": "zone_de_montagne",
#     "Informations générales - Superficie de l'année N": "superficie",
#     "Dotation de solidarité rurale - Fraction péréquation - Population 3 à 16 ans": "population_enfants",
# }


def delete_unecessary_columns(data, columns_to_keep):
    """Supprime les colonnes inutiles de la dataframe pour l'API du MVP"""
    data = data[list(columns_to_keep.keys())]
    return data


def rename_columns(data, columns_to_rename):
    """Renome les colonnes de data avec les noms de variables openfisca
    La table de renommage est fixée dans ce fichier python
    Arguments :
    - data : dataframe contenant les données de la DGCL
    """

    data.rename(
        columns={
            old_name: new_name for old_name, new_name in columns_to_rename.items()
        },
        inplace=True,
    )
    return data


def dsr_calculation(data):
    """Permet de calculer la dotation de solidarité rurale pour chaque commune.
    Arguments :
    - data : dataframe contenant les données de la DGCL avec colonnes nommées avec les noms de variables openfisca
    Retourne :
    - data : la même dataframe avec une colonne en plus contenant le montant de la DSR
    """
    data["dotation_solidarite_rurale"] = (
        data["dsr_fraction_bourg_centre"]
        + data["dsr_fraction_perequation"]
        + data["dsr_fraction_cible"]
    )
    return data


def convert_cols_to_real_bool(data):
    """Convertit les colonnes contenant des "OUI" ou "NON" en vrai booléens "True" ou "False"
    La liste des colonnes est fixée "en dur" dans la fonction
    Arguments :
    - data : dataframe contenant les données de la DGCL avec colonnes nommées avec les noms de variables openfisca
    Retourne :
    - data : la même dataframe avec des colonnes contenant des vrais booléens"""
    columns_list_to_convert = ["zone_de_montagne"]
    column = "zone_de_montagne"
    for col in columns_list_to_convert:
        if is_string_dtype(data[column]):
            data[col] = data[col].str.contains(pat="oui", case=False)
        else:
            data[col] = data[col].astype(bool)
    return data


def load_dgcl_data(path, year):
    data = load_dgcl_file(path)

    if year == 2022:
        data = delete_unecessary_columns(data, columns_to_keep_2022)
        data = rename_columns(data, columns_to_keep_2022)
    elif year == 2021:
        data = delete_unecessary_columns(data, columns_to_keep_2021)
        data = rename_columns(data, columns_to_keep_2021)
    else:
        raise ValueError("Year must be 2021 or 2022, others years are not supported")
    data = dsr_calculation(data)
    data = convert_cols_to_real_bool(data)
    return data

# ---
# Pour data_exploration/dotations_2021/simulation_2021.ipynb
# ajoute ci-dessous les fonctions de : 
# https://gitlab.com/incubateur-territoires/startups/dotations-locales/dotations-locales-back/-/blob/14282d87b8b9198f3a4002a56549088af91b7999/dotations_locales_back/simulation/load_dgcl_data.py
# ---

# imports utilisés via 'eval'
from data_exploration.utils.mapping.criteres_dgcl_2021 import variables_openfisca_presentes_fichier_2021, colonnes_utiles_2021

# Quelques noms de colonne utiles:
elig_bc_dgcl = "Eligible fraction bourg-centre selon DGCL"
elig_pq_dgcl = "Eligible fraction péréquation selon DGCL"
elig_cible_dgcl = "Eligible fraction cible selon DGCL"
code_comm = "Informations générales - Code INSEE de la commune"
nom_comm = "Informations générales - Nom de la commune"

path_assets = os.getcwd() + "/../../data/"

def ajoute_population_plus_grande_commune_agglomeration(
    variables_openfisca_presentes_fichier, data, plus_grosse_commune
):
    """
    Identifie la plus grande commune d'une agglomération en terme de nombre d'habitants
    et l'ajoute au dataframe 'data' fourni en entrée à la colonne 'plus_grosse_commune'.

    Le fichier DGCL ne contient pas d'identifiant d'agglomération par commune.
    Les agglomérations sont donc reconstituées comme suit :

    1. Nous avons par commune le total de la population de l'agglomération à laquelle elle appartient.
    Nous regroupons les communes ayant ce même nombre de population d'agglomération.

    2. Ensuite, par groupe de commune ainsi organisé, nous vérifions qu'en sommant la population
    spécifique à chaque commune, nous ne dépassons pas la population de l'agglomération.
    Ainsi, nous nous assurons que nous n'avons pas créé de groupe de plusieurs agglomérations
    qui, par hasard, auraient eu la même taille de population.
    Ce cas survient sur quelques données DGCL 2019.

    3. On nettoie les agglomérations mal reconstituées.
    En particulier, on neutralise la population de la plus grande commune de ces agglomérations.
    """
    # 1.
    # Niveau ceinture blanche : on groupe by la taille totale de l'agglo (c'est ce qu'on fait icis)
    # À venir : Niveau ceinture orange : en plus de ce critère, utiliser des critères géographiques pour localiser les agglos.
    # À venir : Ceinture rouge : on trouve des données sur les agglos de référence de chaque commune
    pop_agglo = variables_openfisca_presentes_fichier["population_dgf_agglomeration"]
    pop_dgf = variables_openfisca_presentes_fichier["population_dgf"]
    max_pop_plus_grosse_commune = data.groupby(pop_agglo)[
        [pop_dgf]
    ].max()  # index = pop_agglo / lignes en agglo à vérifier

    # 2.
    # Ca marche car le plus haut nombre d'habitants à être partagé par 2 agglo est 23222
    # print(somme_pop_plus_grosse_commune[somme_pop_plus_grosse_commune.index!=somme_pop_plus_grosse_commune[somme_pop_plus_grosse_commune.columns[0]]])

    # 3.
    max_pop_plus_grosse_commune.columns = [plus_grosse_commune]
    max_pop_plus_grosse_commune.loc[
        max_pop_plus_grosse_commune.index == 0, plus_grosse_commune
    ] = 0
    return data.merge(
        max_pop_plus_grosse_commune, left_on=pop_agglo, right_index=True
    ).sort_index()


def ajuste_part_communes_canton(variables_openfisca_presentes_fichier, data, code_comm):
    part_population_canton = variables_openfisca_presentes_fichier[
        "part_population_canton"
    ]
    data[part_population_canton] = data[part_population_canton].replace("nd", 0)
    data[part_population_canton] = data[part_population_canton].astype(float)
    if data.loc[data[code_comm] == "57163", part_population_canton].values[0] >= 0.15:
        data.loc[(data[code_comm] == "57163"), part_population_canton] -= 0.0001
    if data.loc[(data[code_comm] == "87116"), part_population_canton].values[0] >= 0.15:
        data.loc[(data[code_comm] == "87116"), part_population_canton] -= 0.0001
    return data


def ajoute_appartenance_outre_mer(data, outre_mer_dgcl):
    """
    Pour chaque commune, détermine son appartenance à l'outre-mer d'après son département
    et l'ajoute au dataframe 'data' à la colonne de nom désigné par 'outre_mer_dgcl'.
    """
    departement = "Informations générales - Code département de la commune"
    data[departement] = data[departement].astype(str)
    data[outre_mer_dgcl] = (
        (data[departement].str.len() > 2)
        & (~data[departement].str.contains("A"))
        & (~data[departement].str.contains("B"))
    )
    return data


def ajoute_est_chef_lieu_canton(data, is_chef_lieu_canton, code_comm, colonnes_utiles):
    chef_lieu_de_canton_dgcl = colonnes_utiles["chef_lieu_de_canton_dgcl"]
    data[is_chef_lieu_canton] = data[chef_lieu_de_canton_dgcl] == data[code_comm]
    return data


def ajoute_population_chef_lieu_canton(
    data,
    pop_dgf,
    pop_dgf_chef_lieu_canton,
    code_comm,
    colonnes_utiles,
):
    chef_lieu_de_canton_dgcl = colonnes_utiles["chef_lieu_de_canton_dgcl"]

    table_chef_lieu_canton = data[[code_comm, pop_dgf]]
    table_chef_lieu_canton.columns = [code_comm, pop_dgf_chef_lieu_canton]

    data = data.merge(
        table_chef_lieu_canton,
        left_on=chef_lieu_de_canton_dgcl,
        right_on=code_comm,
        how="left",
        suffixes=("", "_Chef_lieu"),
    )
    data[pop_dgf_chef_lieu_canton] = (
        data[pop_dgf_chef_lieu_canton].fillna(0).astype(int)
    )

    return data


def corrige_revenu_moyen_strate(
    data, variables_openfisca_presentes_fichier, revenu_moyen_strate, outre_mer_dgcl
):
    # Certains revenus moyens de communes sont missing...
    # pour ceci, calculons le revenu moyen de chaque strate
    strate = variables_openfisca_presentes_fichier["strate_demographique"]
    revenu_total_dgcl = variables_openfisca_presentes_fichier["revenu_total"]
    pop_insee = variables_openfisca_presentes_fichier["population_insee"]

    data[revenu_total_dgcl] = data[revenu_total_dgcl].replace("nd", 0)
    data[revenu_total_dgcl] = data[revenu_total_dgcl].astype(float)
    tableau_donnees_par_strate = (
        data[(~data[outre_mer_dgcl])]
        .groupby(strate)[[pop_insee, revenu_total_dgcl]]
        .sum()
    )
    tableau_donnees_par_strate[revenu_moyen_strate] = (
        tableau_donnees_par_strate[revenu_total_dgcl]
        / tableau_donnees_par_strate[pop_insee]
    )

    # Avant de corriger les revenus, il nous faut calculer les revenus moyens par strate
    # Les revenus de certaines communes sont ignorés dans le calcul du revenu moyen de la strate, on sait pas pourquoi
    # (ptet la DGCL préserve le secret statistique dans ses metrics agrégées?)
    # La conséquence de la ligne de code qui suit est qu'on utilise la même méthodo que la DGCL
    # donc on a un classement cible plus proche de la vérité.
    # L'inconvénient est que les revenus moyens par strate ne sont pas reproductibles en l'état
    # On pourrait rajouter une variable dans Openfisca qui dirait "est ce que cette commune est
    # prise en compte dans la moyenne de revenu moyen par strate?" , mais le calcul de cette variable
    # n'est pas dans la loi.
    return data.merge(
        tableau_donnees_par_strate[[revenu_moyen_strate]],
        left_on=strate,
        right_index=True,
    )

# ATTENTION FORMULE A VALIDER
def corrige_potentiel_moyen_strate(
    data, variables_openfisca_presentes_fichier, outre_mer_dgcl
):
    # Le potentiel moyen par habitant de la strate n'est pas fourni dans le tableau DGCL 2022, donc on le recalcule
    strate = variables_openfisca_presentes_fichier["strate_demographique"]
    potentiel_financier = variables_openfisca_presentes_fichier["potentiel_financier"]
    pop_dgf = variables_openfisca_presentes_fichier["population_dgf"]

    data[potentiel_financier] = data[potentiel_financier].replace("nd", 0)
    data[potentiel_financier] = data[potentiel_financier].astype(float)
    tableau_donnees_par_strate = (
        data[(~data[outre_mer_dgcl])]
        .groupby(strate)[[pop_dgf, potentiel_financier]]
        .sum()
    )
    tableau_donnees_par_strate["pot_fin_strate"] = (
        tableau_donnees_par_strate[potentiel_financier]
        / tableau_donnees_par_strate[pop_dgf]
    )

    # Avant de corriger les revenus, il nous faut calculer les revenus moyens par strate
    # Les revenus de certaines communes sont ignorés dans le calcul du revenu moyen de la strate, on sait pas pourquoi
    # (ptet la DGCL préserve le secret statistique dans ses metrics agrégées?)
    # La conséquence de la ligne de code qui suit est qu'on utilise la même méthodo que la DGCL
    # donc on a un classement cible plus proche de la vérité.
    # L'inconvénient est que les revenus moyens par strate ne sont pas reproductibles en l'état
    # On pourrait rajouter une variable dans Openfisca qui dirait "est ce que cette commune est
    # prise en compte dans la moyenne de revenu moyen par strate?" , mais le calcul de cette variable
    # n'est pas dans la loi.
    return data.merge(
        tableau_donnees_par_strate[["pot_fin_strate"]],
        left_on=strate,
        right_index=True,
    )


def corrige_revenu_total_commune(
    data,
    variables_openfisca_presentes_fichier,
    colonnes_utiles,
    revenu_moyen_strate: str,
    outre_mer_dgcl,
):
    actual_indice_synthetique = colonnes_utiles["actual_indice_synthetique"]

    pot_fin_par_hab = variables_openfisca_presentes_fichier[
        "potentiel_financier_par_habitant"
    ]

    # ! Attention en 2022, le pot_fin_strate n'est pas fourni dans le tableau de la DGCL
    # On regarde si la colonne existe dans le mapping, si elle n'existe pas on recalcule le potentiel moyen par strate par habitant manuellement
    if colonnes_utiles["pot_fin_strate"] != "":
        pot_fin_strate = colonnes_utiles["pot_fin_strate"]
    else:
        data = corrige_potentiel_moyen_strate(
            data, variables_openfisca_presentes_fichier, outre_mer_dgcl
        )
        pot_fin_strate = "pot_fin_strate"

    # Corrige les infos sur le revenu _total_ de la commune quand il est à 0
    # et que l'indice synthétique est renseigné.
    # Certains revenus _moyens_ de communes sont missing alors qu'ils interviennent dans le calcul de l'indice synthétique...
    revenu_total_dgcl = variables_openfisca_presentes_fichier["revenu_total"]
    pop_insee = variables_openfisca_presentes_fichier["population_insee"]

    # Attention verifier l'équation grâce à la note explicative de la DGCL
    # On essaye de remplir les revenus moyens manquants grâce à notre super equation:
    # RT = pop_insee * (0.3*RMStrate)/(IS-0.7 * PF(strate)/PF)
    data[actual_indice_synthetique] = data[actual_indice_synthetique].astype(float)
    data[pot_fin_strate] = data[pot_fin_strate].astype(float)
    data[pot_fin_par_hab] = data[pot_fin_par_hab].astype(float)
    revenu_moyen_par_habitant_commune = (
        0.3
        * data[revenu_moyen_strate]
        / (
            (data[actual_indice_synthetique] - 0.7)
            * (data[pot_fin_strate] / data[pot_fin_par_hab])
        )
    )
    data.loc[
        (data[revenu_total_dgcl] == 0)
        & (data[pop_insee] > 0)
        & (data[actual_indice_synthetique] > 0),
        revenu_total_dgcl,
    ] = (
        revenu_moyen_par_habitant_commune * data[pop_insee]
    )

    return data


def adapt_dgcl_data(data, year):
    print(year)
    extracolumns = {}
    variables_openfisca_presentes_fichier = eval(
        "variables_openfisca_presentes_fichier_" + year
    )
    colonnes_utiles = eval("colonnes_utiles_" + year)
    #
    # add plus grande commune agglo column
    #
    plus_grosse_commune = "Population plus grande commune de l'agglomération"

    data = ajoute_population_plus_grande_commune_agglomeration(
        variables_openfisca_presentes_fichier, data, plus_grosse_commune
    )
    extracolumns["population_dgf_maximum_commune_agglomeration"] = plus_grosse_commune

    #
    # deux communes ont une part qui apparait >= à 15% du canton mais en fait non.
    # On triche mais pas beaucoup, la part dans la population du canton
    # n'est pas une info facile à choper exactement.
    # Manquant : nombre d'habitants du canton mais nous avons la part population canton (malheureusement arrondie).
    #
    data = ajuste_part_communes_canton(
        variables_openfisca_presentes_fichier, data, code_comm
    )
    #
    # introduit la valeur d'outre mer
    #
    outre_mer_dgcl = "commune d'outre mer"
    data = ajoute_appartenance_outre_mer(data, outre_mer_dgcl)
    extracolumns["outre_mer"] = outre_mer_dgcl

    # Mise des chefs lieux de canton en une string de 5 caractères.
    chef_lieu_de_canton_dgcl = colonnes_utiles["chef_lieu_de_canton_dgcl"]
    data[chef_lieu_de_canton_dgcl] = data[chef_lieu_de_canton_dgcl].apply(
        lambda x: str(x).zfill(5)
    )

    #
    # Chope les infos du chef-lieu de canton
    #
    is_chef_lieu_canton = "Chef-lieu de canton"
    data = ajoute_est_chef_lieu_canton(
        data, is_chef_lieu_canton, code_comm, colonnes_utiles
    )
    extracolumns["chef_lieu_de_canton"] = is_chef_lieu_canton

    pop_dgf = variables_openfisca_presentes_fichier["population_dgf"]
    pop_dgf_chef_lieu_canton = pop_dgf + " du chef-lieu de canton"
    data = ajoute_population_chef_lieu_canton(
        data,
        pop_dgf,
        pop_dgf_chef_lieu_canton,
        code_comm,
        colonnes_utiles,
    )
    extracolumns["population_dgf_chef_lieu_de_canton"] = pop_dgf_chef_lieu_canton

    # Corrige les infos sur le revenu _total_ de la commune quand il est à 0
    # et que l'indice synthétique est renseigné. Certains revenus _moyens_ sont missing...
    # Avant de corriger les revenus, il nous faut calculer les revenus moyens par strate
    revenu_moyen_strate = " Revenu imposable moyen par habitant de la strate"
    data = corrige_revenu_moyen_strate(
        data, variables_openfisca_presentes_fichier, revenu_moyen_strate, outre_mer_dgcl
    )
    extracolumns["revenu_par_habitant_moyen"] = revenu_moyen_strate

    data = corrige_revenu_total_commune(
        data,
        variables_openfisca_presentes_fichier,
        colonnes_utiles,
        revenu_moyen_strate,
        outre_mer_dgcl,
    )

    #
    # Génère le dataframe au format final :
    # Il contient toutes les variables qu'on rentrera dans openfisca au format openfisca
    # + Des variables utiles qu'on ne rentre pas dans openfisca
    #
    # colonnes = colonnes du fichier dgcl qui deviennent les inputs pour les calculs openfisca
    # lignes = 1 ligne par commune
    # Restriction aux colonnes intéressantes :

    rang_indice_synthetique = colonnes_utiles["rang_indice_synthetique"]
    montant_commune_eligible = colonnes_utiles["montant_commune_eligible"]
    part_pfi = colonnes_utiles["part_pfi"]

    translation_cols = {**variables_openfisca_presentes_fichier, **extracolumns}
    data[elig_bc_dgcl] = data[montant_commune_eligible] > 0
    data[elig_pq_dgcl] = data[part_pfi] > 0
    data[elig_cible_dgcl] = (data[rang_indice_synthetique] > 0) & (
        data[rang_indice_synthetique] <= 10000
    )
    # Au delà des colonnes traduites, on garde ces colonnes dans le dataframe de sortie.
    # Pour garder des informations pour identifier la commune
    autres_cols_interessantes = [code_comm, nom_comm]
    data = data[autres_cols_interessantes + list(translation_cols.values())]

    # Renomme colonnes
    invert_dict = {
        name_dgcl: name_ofdl for name_ofdl, name_dgcl in translation_cols.items()
    }
    data.columns = [
        column if column not in invert_dict else invert_dict[column]
        for column in data.columns
    ]
    data["chef_lieu_departement_dans_agglomeration"] = (
        data["chef_lieu_departement_dans_agglomeration"].replace("nd", "0").copy()
    )
    data["chef_lieu_departement_dans_agglomeration"] = data[
        "chef_lieu_departement_dans_agglomeration"
    ].astype(int)

    # Passe les "booléens dgf" (oui/non) en booléens normaux
    liste_columns_to_real_bool = [
        "bureau_centralisateur",
        "chef_lieu_arrondissement",
        "chef_lieu_de_canton",
        "chef_lieu_departement_dans_agglomeration",
        "zrr",
        "zone_de_montagne",
        "insulaire",
    ]

    data = convert_cols_to_real_bool_2022(data, liste_columns_to_real_bool)

    # Convertit les colonnes de string vers Float
    liste_columns_to_float = [
        "effort_fiscal",
        "superficie",
        "recettes_reelles_fonctionnement",
        "potentiel_fiscal",
        "population_dgf_majoree",
    ]
    for col in liste_columns_to_float:
        data[col] = data[col].astype(float)

    columns_replace_nd = [
        "population_dgf_agglomeration",
        "population_dgf_departement_agglomeration",
        "longueur_voirie",
        "population_enfants",
        "nombre_beneficiaires_aides_au_logement",
    ]
    for col in columns_replace_nd:
        data[col] = data[col].replace("nd", 0).copy()
        data[col] = data[col].astype(float)

    # Convertit les colonnes de string vers int
    liste_columns_to_int = [
        "population_dgf_agglomeration",
        "population_dgf_departement_agglomeration",
        "population_enfants",
        "nombre_beneficiaires_aides_au_logement",
    ]
    for col in liste_columns_to_int:
        data[col] = data[col].astype(int)

    return data.sort_index()


def insert_dsr_garanties_communes_nouvelles(data, period="2022", folder=path_assets):
    filename = folder + "garanties_cn_dsr_{}.csv".format(period)
    data_garanties = pandas.read_csv(filename, dtype={code_comm: str})
    colonnes_a_ajouter = [
        code_comm,
        "dsr_garantie_commune_nouvelle_fraction_bourg_centre",
        "dsr_garantie_commune_nouvelle_fraction_perequation",
        "dsr_garantie_commune_nouvelle_fraction_cible",
    ]
    data = data.merge(data_garanties[colonnes_a_ajouter], how="left", on=code_comm)
    for k in colonnes_a_ajouter:
        data[k] = data[k].fillna(0)
    return data


# ---
# Pour data_exploration/dotations_2022/analyse_2022.ipynb
# Source : https://gitlab.com/incubateur-territoires/startups/dotations-locales/dotations-locales-back/-/blob/14282d87b8b9198f3a4002a56549088af91b7999/dotations_locales_back/simulation/simulation.py
# ---

from data_exploration.utils.lecture_data import CRITERES_2022_PATH, GARANTIES_DSU_PATH
from data_exploration.utils.load_dgcl_criteres_data import insert_dsu_garanties
from data_exploration.utils.utils import load_dgcl_file

# import utilisé via 'eval'
from data_exploration.utils.mapping.criteres_dgcl_2021 import variables_calculees_an_dernier_2021
from data_exploration.utils.mapping.criteres_dgcl_2022 import variables_openfisca_presentes_fichier_2022, colonnes_utiles_2022


# Attention revérifier si les années sont bien gérées.
def get_last_year_dotations(data, year):
    # renvoie un DataFrame qui contient les colonnes :
    # code commune : avec le nom original car on n'a toujours pas de variable OFDL
    # des variables de RESULTATS tels que calculés par la DGCL.
    # Ces variables portent leur nom openfisca parce que bon on va pas se trimballer partout
    # les noms du fichier (à part pour leur code commune bien sûr)
    variables_calculees_an_dernier = eval("variables_calculees_an_dernier_" + year)

    resultats_extraits = data[[code_comm]]

    # Ajout de variables qui n'existent pas à l'état brut dans le fichier :

    # L'éligibilité est déterminée en fonction de la présence ou non d'un versement non nul
    # Ajout des variables de résultat présentes à l'état brut dans le fichier
    for nom_dgcl, nom_ofdl in variables_calculees_an_dernier.items():
        resultats_extraits[nom_ofdl] = data[nom_dgcl]
    resultats_extraits["dsu_montant_eligible"] = (
        resultats_extraits["dsu_part_spontanee"]
        + resultats_extraits["dsu_part_augmentation"]
    )

    resultats_extraits["dsr_montant_hors_garanties_fraction_perequation"] = data[
        [
            nom_colonne
            for nom_colonne in variables_calculees_an_dernier.keys()
            if "Dotation de solidarité rurale - Péréquation - Part" in nom_colonne
        ]
    ].sum(axis="columns")
    resultats_extraits["dsr_montant_eligible_fraction_perequation"] = (
        resultats_extraits["dsr_montant_hors_garanties_fraction_perequation"] > 0
    ) * resultats_extraits["dsr_fraction_perequation"]

    resultats_extraits["dsr_montant_hors_garanties_fraction_cible"] = data[
        [
            nom_colonne
            for nom_colonne in variables_calculees_an_dernier.keys()
            if "Dotation de solidarité rurale - Cible - Part" in nom_colonne
        ]
    ].sum(axis="columns")

    assert "dsr_montant_eligible_fraction_bourg_centre" in resultats_extraits.columns
    return resultats_extraits


def convert_cols_to_real_bool_2022(data, bool_col_list):
    """Convertit les colonnes contenant des "OUI" ou "NON" en vrai booléens "True" ou "False"
    La liste des colonnes est fixée "en dur" dans la fonction
    Arguments :
    - data : dataframe contenant les données de la DGCL avec colonnes nommées avec les noms de variables openfisca
    Retourne :
    - data : la même dataframe avec des colonnes contenant des vrais booléens"""

    for col in bool_col_list:
        if is_string_dtype(data[col]):

            # Les colonnes qui contiennent des "OUI" "NON"
            if "OUI" in data[col].values:
                data[col] = data[col].str.contains(pat="oui", case=False)

            # Les colonnes qui contiennent soit 1 soit le code commune lorsque vrai
            else:
                data[col] = data[col].replace("nd", "0").copy()
                data[col].replace(to_replace=r"^\d{5}$", value="1", regex=True)
                data[col] = data[col].astype(bool)

        # Les colonnes qui contiennent soit 0 ou 1 et de type int
        else:
            data[col] = data[col].astype(bool)
    return data


CURRENT_YEAR = 2022
dgcl_data_2022 = load_dgcl_file(CRITERES_2022_PATH)
adapted_data_2022 = adapt_dgcl_data(dgcl_data_2022, "2022")

# ajout des garanties dsu toutes périodes (2019 -> 2022)
# insertion des garanties 2022 au titre de la DSU (non calculées explicitement dans OFDL)

fully_adapted_data_2022 = insert_dsu_garanties(
    adapted_data_2022, str(CURRENT_YEAR), GARANTIES_DSU_PATH
)

# insertion des garanties communes nouvelles au titre de la DSR (non calculées explicitement dans OFDL)
# TODO attention ! corriger ; par défaut, renvoie les garanties 2020 si l'année demandée > 2020
fully_adapted_data_2022 = insert_dsr_garanties_communes_nouvelles(
    fully_adapted_data_2022, str(CURRENT_YEAR)
)
