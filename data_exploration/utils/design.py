from pandas import IndexSlice
from numpy import isclose


def highlight_une_colonne(df, nom_colonne):
    def highlight_cols(x):
        df = x.copy()

        # applique une couleur par défaut à toutes les valeurs
        df.loc[:,:] = 'background-color: white'

        # surcharge les valeurs d'une colonne
        df[[nom_colonne]] = 'background-color: #BAE6F5'
    
        return df    

    return df.style.apply(highlight_cols, axis=None)

def highlight_comparaison_une_colonne(df, nom_colonne_base, nom_colonne_comparee, precision_euros=0):
    '''
    Pour un dataframe, surligne en couleur les cellules de nom_colonne_comparee dont la valeur diffère de nom_colonne_base.
    En rouge quand la colonne comparée a un montant supérieur à la précision indiquée près.
    En jaune quand la colonne comparée a un montant inférieur à la précision indiquée près.
    En gris quand la colonne comparée est équivalente à la colonne de base à la précision indiquée près.
    L'égalité parfaite n'est pas colorée.
    '''
    def highlight_3(x):
        def conditions(x, v):
            if v - x.loc[nom_colonne_base] >= precision_euros :
                return "background: red"
            elif x.loc[nom_colonne_base] - v >= precision_euros :
                return "background: yellow"
            elif x.loc[nom_colonne_base] == v: 
                return ""
            else:
                # egaux à la précision près
                return "background: #E5E4E2"

        return [conditions(x, v) for v in x]

    # on définit un subset de colonnes dont le type est adapté aux conditions de highlighting (nombres ici)
    return df.style.apply(highlight_3, axis = 1, subset=IndexSlice[:, [nom_colonne_base, nom_colonne_comparee]])
