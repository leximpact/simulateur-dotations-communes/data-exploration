from os import getcwd
from os.path import join

from openfisca_france_dotations_locales import CountryTaxBenefitSystem as OpenFiscaFranceDotationsLocales

from leximpact_dotations_back.calculate import create_simulation_with_data
from leximpact_dotations_back.data_building.adapt_dotations_criteres import adapt_criteres
from leximpact_dotations_back.data_building.build_dotations_data import get_previous_year_data, load_criteres


CURRENT_YEAR = 2024
DATA_DIRECTORY = join(getcwd(), "..", "..", "data")


def buid_data_2023_for_2024(data_directory = DATA_DIRECTORY, year = CURRENT_YEAR):
    data = get_previous_year_data(year, data_directory)  # pour récupérer year - 1
    return data


def build_data_2024(data_directory = DATA_DIRECTORY, year = CURRENT_YEAR):
    # construction des données reproduisant
    # leximpact_dotations_back.data_building.build_dotations_data.build_data
    # pour permettre d'éventuels tests intermédiaires
    
    # hypothèse : le fichier de critères est nommé 'criteres_repartition_YYYY.csv'
    # où YYYY est défini par la variable 'year'
    data_criteres = load_criteres(data_directory, year)
    data = adapt_criteres(data_criteres, year)
    return data


def create_simulation_2024(data, data_previous_year, year=CURRENT_YEAR):
    model = OpenFiscaFranceDotationsLocales()

    data_criteres = load_criteres(DATA_DIRECTORY, year)
    data = adapt_criteres(data_criteres, year)

    return create_simulation_with_data(model, year, data, data_previous_year)


def calculate_dotations(simulation, dotations_criteres_data, year = CURRENT_YEAR):
    
    # DF

    dotation_forfaitaire = simulation.calculate(
        "dotation_forfaitaire", year
    )
    
    # DSR
    
    dsr_fraction_bourg_centre = simulation.calculate(
        "dsr_fraction_bourg_centre", year
    )
    dsr_fraction_perequation = simulation.calculate(
        "dsr_fraction_perequation", year
    )
    dsr_fraction_cible = simulation.calculate(
        "dsr_fraction_cible", year
    )
    
    dotation_solidarite_rurale = simulation.calculate("dotation_solidarite_rurale", year)

    # DSU
    
    dsu_montant = simulation.calculate("dsu_montant", year)

    # DCN
    
    dotation_communes_nouvelles_part_amorcage = simulation.calculate("dotation_communes_nouvelles_part_amorcage", year)
    dotation_communes_nouvelles_part_garantie = simulation.calculate("dotation_communes_nouvelles_part_garantie", year)

    dotation_communes_nouvelles = simulation.calculate(
        "dotation_communes_nouvelles", year
    )

    # stockage des résultats même si accès aussi possible via
    # simulation.commune.get_holder('dotation_forfaitaire').get_array(2024)

    simulated_data = dotations_criteres_data.copy()

    simulated_data["dotation_forfaitaire"] = dotation_forfaitaire
    
    simulated_data["dsu_montant"] = dsu_montant
    
    simulated_data["dsr_fraction_bourg_centre"] = dsr_fraction_bourg_centre
    simulated_data["dsr_fraction_perequation"] = dsr_fraction_perequation
    simulated_data["dsr_fraction_cible"] = dsr_fraction_cible
    simulated_data["dotation_solidarite_rurale"] = dotation_solidarite_rurale

    simulated_data["dotation_communes_nouvelles_part_amorcage"] = dotation_communes_nouvelles_part_amorcage
    simulated_data["dotation_communes_nouvelles_part_garantie"] = dotation_communes_nouvelles_part_garantie
    simulated_data["dotation_communes_nouvelles"] = dotation_communes_nouvelles

    return simulated_data
