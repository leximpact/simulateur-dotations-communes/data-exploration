# données pour un calcul sur l'année N :
#   critères année N
#   critères année N-1
#   ...


# PREMIERE TENTATIVE D'APPEL À LA SIMULATION = SIMU API WEB
# ---
# PROBLEMES :
# simulated_data n'a pas les garanties
# fully_adapted_data_2022 n'a que la dsu_montant_garantie_pluriannuelle (pas de part spontanée etc.)
# ---
# from dotations_locales_back.simulation.simulation import calculate_dotations, fully_adapted_data_2022
# from dotations_locales_back.web_api.main import dotations_criteres_2022
#
# simulated_data = calculate_dotations(fully_adapted_data_2022, dotations_criteres_2022)
#
# mais simulated_data ne contient pas les garanties
# d'après : dotations_locales_back/simulation/simulation.py::calculate_dotations
# dotations_criteres_2022 : les données de critères qui sont conservées dans simulated_data
#                           ('columns_to_keep_2022' seulement)
# fully_adapted_data_2022 : les données avec lesquelles on initialise la simulation 
#                           (critères et autres calculs)
#
# fully_adapted_data_2022
# Index(['Informations générales - Code INSEE de la commune',
#        'Informations générales - Nom de la commune', 'bureau_centralisateur',
#        'chef_lieu_arrondissement', 'chef_lieu_de_canton',
#        'chef_lieu_departement_dans_agglomeration', 'part_population_canton',
#        'population_dgf', 'population_dgf_agglomeration',
#        'population_dgf_departement_agglomeration', 'population_insee',
#        'potentiel_financier', 'potentiel_financier_par_habitant',
#        'revenu_total', 'strate_demographique', 'zrr', 'effort_fiscal',
#        'longueur_voirie', 'zone_de_montagne', 'insulaire', 'superficie',
#        'population_enfants', 'nombre_logements', 'nombre_logements_sociaux',
#        'nombre_beneficiaires_aides_au_logement', 'population_qpv',
#        'population_zfu', 'population_dgf_majoree',
#        'recettes_reelles_fonctionnement', 'potentiel_fiscal',
#        'population_dgf_maximum_commune_agglomeration', 'outre_mer',
#        'population_dgf_chef_lieu_de_canton', 'revenu_par_habitant_moyen',
#        'dsu_montant_garantie_pluriannuelle',
#        'dsr_garantie_commune_nouvelle_fraction_bourg_centre',
#        'dsr_garantie_commune_nouvelle_fraction_perequation',
#        'dsr_garantie_commune_nouvelle_fraction_cible'],
#       dtype='object')


# DEUXIEME TENTATIVE D'APPEL À LA SIMULATION = SIMU API WEB
# contournement de dotations_locales_back/simulation/simulation.py::calculate_dotations
# https://gitlab.com/incubateur-territoires/startups/dotations-locales/dotations-locales-back/-/blob/8e5c2edc5c6dfdd24e00314c6a7cd5b61d5034e1/dotations_locales_back/simulation/simulation.py#L148
# on extrait la simulation dans create_simulation_2022


from openfisca_core.simulation_builder import SimulationBuilder
from openfisca_france_dotations_locales import (
    CountryTaxBenefitSystem as DotationsLocales,
)
from data_exploration.utils.lecture_data import criteres_repartition_2021
from data_exploration.utils.new_load_dgcl_criteres_data import get_last_year_dotations, load_dgcl_file


CURRENT_YEAR = 2022
tbs = DotationsLocales()


# source : https://gitlab.com/incubateur-territoires/startups/dotations-locales/dotations-locales-back/-/blob/14282d87b8b9198f3a4002a56549088af91b7999/dotations_locales_back/simulation/simulation.py
dgcl_data_2021 = criteres_repartition_2021
results_2021_as_last_year = get_last_year_dotations(dgcl_data_2021, "2021")
data_2021_as_last_year = results_2021_as_last_year[
    [
        "Informations générales - Code INSEE de la commune",
        "dsu_montant_eligible",
        "dsr_montant_eligible_fraction_bourg_centre",
        "dsr_montant_eligible_fraction_perequation",
        "dsr_montant_hors_garanties_fraction_cible",
        "population_dgf_majoree",
        "dotation_forfaitaire",
    ]
]

from data_exploration.utils.lecture_data import CRITERES_2022_PATH
from data_exploration.utils.new_load_dgcl_criteres_data import load_dgcl_data
dotations_criteres_2022 = load_dgcl_data(CRITERES_2022_PATH, 2022)  # ex from dotations_locales_back.web_api.main import dotations_criteres_2022


# source : https://gitlab.com/incubateur-territoires/startups/dotations-locales/dotations-locales-back/-/blob/14282d87b8b9198f3a4002a56549088af91b7999/dotations_locales_back/simulation/simulation.py#L82
def simulation_from_dgcl_csv(period, data, tbs, data_previous_year=None):
    # fonction importée de leximpact-server : dotations/simulation.py
    sb = SimulationBuilder()
    sb.create_entities(tbs)
    sb.declare_person_entity("commune", data.index)

    nombre_communes = len(data.index)
    etat_instance = sb.declare_entity("etat", ["france"])
    etat_communes = ["france"] * nombre_communes
    sb.join_with_persons(etat_instance, etat_communes, [None] * nombre_communes)

    simulation = sb.build(tbs)
    simulation.max_spiral_loops = 10

    for champ_openfisca in data.columns:
        if " " not in champ_openfisca:  # oui c'est comme ça que je checke
            # qu'une variable est openfisca ne me jugez pas
            simulation.set_input(
                champ_openfisca,
                period,
                data[champ_openfisca],
            )
    # data_previous_year est un dataframe dont toutes les colonnes
    # portent des noms de variables openfisca
    # et contiennent des valeurs de l'an dernier.
    if data_previous_year is not None:
        # on rassemble les informations de l'an dernier pour les communes
        # qui existent cette année (valeurs nouvelles communes à zéro)
        data = data.merge(
            data_previous_year,
            on="Informations générales - Code INSEE de la commune",
            how="left",
            suffixes=["_currentyear", ""],
        )
        for champ_openfisca in data_previous_year.columns:
            if " " not in champ_openfisca:  # oui c'est comme ça que je checke
                # qu'une variable est openfisca ne me jugez pas
                simulation.set_input(
                    champ_openfisca,
                    str(int(period) - 1),
                    data[champ_openfisca].fillna(0),
                )
    return simulation


# from dotations_locales_back.simulation.simulation import fully_adapted_data_2022 as simulation_data
def create_simulation_2022(simulation_data):
    simulation_2022 = simulation_from_dgcl_csv(
        CURRENT_YEAR,
        simulation_data,
        tbs,
        data_previous_year=data_2021_as_last_year,
    )
    return simulation_2022


# def calculate_dotations(simulation_data, dotations_criteres_data):
#    simulation_2022 = create_simulation_2022(simulation_data)
#    ...
def calculate_dotations(simulation, dotations_criteres_data, year = CURRENT_YEAR):
    dotation_forfaitaire = simulation.calculate(
        "dotation_forfaitaire", year
    )
    dsu_montant = simulation.calculate("dsu_montant", year)
    dsr_fraction_bourg_centre = simulation.calculate(
        "dsr_fraction_bourg_centre", year
    )
    dsr_fraction_perequation = simulation.calculate(
        "dsr_fraction_perequation", year
    )
    dsr_fraction_cible = simulation.calculate("dsr_fraction_cible", year)
    simulated_data = dotations_criteres_data.copy()
    simulated_data["dotation_forfaitaire"] = dotation_forfaitaire
    simulated_data["dsu_montant"] = dsu_montant
    simulated_data["dsr_fraction_bourg_centre"] = dsr_fraction_bourg_centre
    simulated_data["dsr_fraction_perequation"] = dsr_fraction_perequation
    simulated_data["dsr_fraction_cible"] = dsr_fraction_cible
    simulated_data["dotation_solidarite_rurale"] = (
        simulated_data["dsr_fraction_bourg_centre"]
        + simulated_data["dsr_fraction_perequation"]
        + simulated_data["dsr_fraction_cible"]
    )
    return simulated_data
