from os import getcwd
from numpy import setdiff1d
from pandas import concat, DataFrame, read_csv


# ANALYSE DES ECARTS ENTRE LISTE DE TERRITOIRES DGCL 2021 et 2022
# OBJET : IDENTIFIER LES ETAPES DE CONSTRUCTION DE CES LISTES POUR EVENTUELLE RECONSTRUCTION EN 2023


# LISTE DES DONNEES
DATA_DIRECTORY = getcwd() + "/data/territoires/"


DATA_COMMUNES_2021_DGF_PATH = DATA_DIRECTORY + "communes_2021_dotation_globale_fonctionnement.csv"  # DGCL site
DATA_COMMUNES_2022_DGF_PATH = DATA_DIRECTORY + "communes_2022_dotation_globale_fonctionnement.csv"  # DGCL site

DATA_COMMUNES_FILIATION_2022_2021_PATH = DATA_DIRECTORY + "filiation_communes_2022-2021.csv"  # DGCL mail
DATA_COMMUNES_EVENEMENTS_2022_PATH = DATA_DIRECTORY + "insee_mvtcommune_2022.csv"  # https://www.insee.fr/fr/information/6051727 [13739 rows x 14 columns]

DATA_OUTPUT_DIRECTORY = getcwd() + "/tmp_outputs/"

COMMUNE_CODE_INSEE_PATTERN = '([0-9A-Z]{5})\s-\s(.*)'


# CHARGEMENT DES DONNEES

communes_2021_dgf=read_csv(
    DATA_COMMUNES_2021_DGF_PATH,
    dtype={
        "D.G.F. montant total - 2021": str,
        "": str
        },
    encoding="utf-8",
    sep=';'
    )

communes_2022_dgf=read_csv(
    DATA_COMMUNES_2022_DGF_PATH, 
    dtype={
        "D.G.F. montant total - 2022": str,
        "": str
        },
    encoding="utf-8",
    sep=';'
    )


communes_filiation_2022_2021=read_csv(
    DATA_COMMUNES_FILIATION_2022_2021_PATH, 
    dtype={
        "Code INSEE de la commune 2021": str,
        "Nom de la commune 2021": str,
        "Code INSEE de la commune 2022": str,
        "Nom de la commune 2022": str,
        "Date de l'arrêté préfectoral": str
        },
    encoding="utf-8",
    sep=','
    )

communes_evenements_insee_2022=read_csv(
    DATA_COMMUNES_EVENEMENTS_2022_PATH, 
    sep=','
    )

# ECARTS COMMUNES DGCL 2021-2022

def containsNaN(df):
    return df.isnull().any().any()


def voir_ecarts_dgcl_dgf(communes_2021_dgf, communes_2022_dgf, communes_filiation_2022_2021, communes_evenements_insee_2022):
    merged_2022_2021 = communes_2022_dgf.merge(
        communes_2021_dgf,
        indicator = True,
        left_on=["D.G.F. montant total - 2022"],
        right_on=["D.G.F. montant total - 2021"],
        how = 'outer'
        )

    # on élimine les communes identiques en 2021 et 2022 (code et nom)
    diff_2022_2021 = merged_2022_2021.loc[lambda x : x['_merge'] != 'both']  # Remove shared rows

    diff_2022_2021_split = DataFrame(columns=['code_insee_2021', 'nom_commune_2021', 'code_insee_2022', 'nom_commune_2022'])
    diff_2022_2021_split[['code_insee_2021', 'nom_commune_2021']] = diff_2022_2021["D.G.F. montant total - 2021"].str.extract(COMMUNE_CODE_INSEE_PATTERN, expand = True)
    diff_2022_2021_split[['code_insee_2022', 'nom_commune_2022']] = diff_2022_2021["D.G.F. montant total - 2022"].str.extract(COMMUNE_CODE_INSEE_PATTERN, expand = True)
    print(diff_2022_2021_split)

    # on élimine les communes nouvelles et leurs ancêtres

    filtre_filiation_2021_ancetres = ~diff_2022_2021_split['code_insee_2021'].isin(communes_filiation_2022_2021['Code INSEE de la commune 2021'])
    filtre_filiation_2022_nouvelles = ~diff_2022_2021_split['code_insee_2022'].isin(communes_filiation_2022_2021['Code INSEE de la commune 2022'])
    diff_2022_2021_sans_filiation = diff_2022_2021_split[filtre_filiation_2021_ancetres & filtre_filiation_2022_nouvelles]
    print(diff_2022_2021_sans_filiation)

    # on identifie puis élimine les renommages simples

    DATE_EFFET = '2022-01-01'  # antérieure à l'évènement (création ?)
    filtre_effet_2022 = communes_evenements_insee_2022['DATE_EFF'].astype(str) == DATE_EFFET
    MODALITE_EVENEMENT_CHANGEMENT_NOM = 10  # https://www.insee.fr/fr/information/5057793#mod
    filtre_changement_nom = communes_evenements_insee_2022['MOD'].astype(int) == MODALITE_EVENEMENT_CHANGEMENT_NOM
    renommages_effet_2022 = communes_evenements_insee_2022[filtre_effet_2022 & filtre_changement_nom]
    print(renommages_effet_2022)  # identification renommages

    filtre_renommages_effet_2022_ancetres = ~diff_2022_2021_sans_filiation['code_insee_2021'].isin(renommages_effet_2022['COM_AV'])
    filtre_renommages_effet_2022_nouvelles = ~diff_2022_2021_sans_filiation['code_insee_2022'].isin(renommages_effet_2022['COM_AP'])
    diff_2022_2021_sans_filiation_renommage = diff_2022_2021_sans_filiation[filtre_renommages_effet_2022_ancetres & filtre_renommages_effet_2022_nouvelles]
    print(diff_2022_2021_sans_filiation_renommage) # écarts restant à expliquer (filialtion et renommages retirés)

    # diff_2022_2021_sans_filiation_renommage.to_csv(DATA_OUTPUT_DIRECTORY + "diff_2022_2021.csv")



def tente_reconstitution_dgcl_dgf_2022(communes_2021_dgf, communes_2022_dgf, communes_filiation_2022_2021):
    print(communes_2022_dgf)
    # split le format pour faciliter la comparaison (extrait code et nom de "code - nom")
    communes_2022_dgf_split = DataFrame(
        columns=['code_insee_2022', 'nom_commune_2022'],
        dtype=str
        )
    communes_2022_dgf_split[['code_insee_2022', 'nom_commune_2022']] = communes_2022_dgf["D.G.F. montant total - 2022"].str.extract(COMMUNE_CODE_INSEE_PATTERN, expand = True)

    # démarre 2022 de la liste des communes 2021 
    communes_2022_dgf_reconstruit = DataFrame(columns=['code_insee_2022', 'nom_commune_2022'], dtype=str)

    communes_2021_dgf_split = DataFrame(columns=['code_insee_2021', 'nom_commune_2021'])
    communes_2021_dgf_split[['code_insee_2021', 'nom_commune_2021']] = communes_2021_dgf["D.G.F. montant total - 2021"].str.extract(COMMUNE_CODE_INSEE_PATTERN, expand = True)
    assert not containsNaN(communes_2021_dgf)

    # suppression des communes ayant changé en 2022
    assert communes_filiation_2022_2021['Code INSEE de la commune 2021'].isin(communes_2021_dgf_split['code_insee_2021']).all()
    
    communes_2021_sans_nvelles_2022 = communes_2021_dgf_split[
        ~communes_2021_dgf_split['code_insee_2021'].isin(
            communes_filiation_2022_2021['Code INSEE de la commune 2021'])
            ]
    
    # vérifie l'absence de NaN (si erreur de filtre ou typo sur code commune)
    assert not containsNaN(communes_2021_dgf_split)
    assert not containsNaN(communes_2021_sans_nvelles_2022)
    
    communes_2022_dgf_reconstruit[['code_insee_2022', 'nom_commune_2022']] = communes_2021_dgf_split[
        ~communes_2021_dgf_split['code_insee_2021'].isin(
        communes_filiation_2022_2021['Code INSEE de la commune 2021'])
        ]
    assert not containsNaN(communes_2022_dgf_reconstruit)
    assert len(communes_2022_dgf_reconstruit) == len(communes_2021_dgf_split) - len(communes_filiation_2022_2021)
    
    # ajout des communes nouvelles 2022
    communes_2022_dgf_reconstruit = concat([
        communes_2022_dgf_reconstruit, 
        communes_filiation_2022_2021[
            ['Code INSEE de la commune 2022', 'Nom de la commune 2022']
            ].drop_duplicates().rename(columns={  # rename temporaire au concat
                'Code INSEE de la commune 2022': 'code_insee_2022',
                'Nom de la commune 2022': 'nom_commune_2022'
                })
        ]
        )
    
    # ordonnance les lignes par ordre alphabétique de nom de commune tel que dans le fichier DGCL
    communes_2022_dgf_reconstruit.sort_values('nom_commune_2022', inplace=True)
    communes_2022_dgf_reconstruit = communes_2022_dgf_reconstruit.reset_index(drop=True)
    print(communes_2022_dgf_reconstruit.tail(20))

    # vérification des écarts par code insee (écarts de nommage à vérifier plus bas)
    dgcl_communes_not_reconstruit = communes_2022_dgf_split[~ communes_2022_dgf_split['code_insee_2022'].isin(communes_2022_dgf_reconstruit['code_insee_2022'])]
    assert len(dgcl_communes_not_reconstruit) == 0, "Codes insee manquants => communes DGCL à ajouter"

    reconstruit_communes_not_dgcl = communes_2022_dgf_reconstruit[~ communes_2022_dgf_reconstruit['code_insee_2022'].isin(communes_2022_dgf_split['code_insee_2022'])]
    assert len(reconstruit_communes_not_dgcl) == 0, "Codes insee manquants => communes reconstruites à retirer"

    assert len(communes_2022_dgf_split) == len(communes_2022_dgf_reconstruit)
    assert len(setdiff1d(communes_2022_dgf_split['code_insee_2022'].values.astype(str), communes_2022_dgf_reconstruit['code_insee_2022'].values.astype(str))) == 0
    
    # vérification des écarts par nom de commune
    print("DGCL 2022 manquant : ", setdiff1d(communes_2022_dgf_split['nom_commune_2022'].values.astype(str), communes_2022_dgf_reconstruit['nom_commune_2022'].values.astype(str)))
    print("Reconstruit 2022 différent de la DGCL : ", setdiff1d(communes_2022_dgf_reconstruit['nom_commune_2022'].values.astype(str), communes_2022_dgf_split['nom_commune_2022'].values.astype(str)))


# voir_ecarts_dgcl_dgf(communes_2021_dgf, communes_2022_dgf, communes_filiation_2022_2021, communes_evenements_insee_2022)
tente_reconstitution_dgcl_dgf_2022(communes_2021_dgf, communes_2022_dgf, communes_filiation_2022_2021)
