# Sources de données

Les données présentes dans ce dossier sont mises à disposition par la Direction Générale des Collectivités Locales (DGCL). 

Les données utilisées dans ce projet ne concernent que les communes.
On traite de deux sujets : 
* Le calcul des dotations de fonctionnement communales
* La mise à jour de la liste des territoires (communes) nécessaire au calcul annuel des dotations

## Exploration du calcul des dotations communales

Deux types de fichiers de données sont présents : 
* Les fichiers des montants des différentes dotations accordés à chaque commune par année
* Le fichier des critères de répartition utilisés pour le calcul des dotations. Ce fichier comprend aussi les montants des dotations

Les données ont été converties au format csv pour facilité leur manipulation dans le projet. 

### Téléchargement 

Les données sont accessibles et téléchargeables via le [site](http://www.dotations-dgcl.interieur.gouv.fr/consultation/accueil.php) de la DGCL. 

Pour la notification officielle des montants de dotations, la DGCL met à disposition un fichier par type de dotation et par année. 
Ils sont téléchargeables via cette [page](http://www.dotations-dgcl.interieur.gouv.fr/consultation/dotations_en_ligne.php) en choississant l'année et le type de dotation à l'aide des menus déroulants. 

> ⚠️ Le format du fichier obtenu via le bouton `Télécharger le CSV` peut varier d'un navigateur à un autre. Sur Firefox, on obtient un fichier .xlsx alors que sous Safari, une page est produite avec les données brutes comme dans cet extrait des 2 premières lignes la DGF des communes en 2021 : 
> ```csv
> D.G.F. montant total - 2021
> 01001 - ABERGEMENT-CLEMENCIAT	120 534
> ```
> Mais le fichier .xls obtenu sous Firefox est en réalité un fichier texte dont l'encodage par par défaut semble être `Windows-1252/WinLatin 1`.
> Pour éviter les problèmes d'encodage (testé sous macOS), le chemin le plus court semble malheureusement être : 
> * Copier-coller du tableau dans le csv.
> * Remplacer automatiquement toutes les tabulations par un point-virgule.
> * Définir UTF-8 comme encodage par défaut sur VSCode.

Pour les critères de répartition, la DGCL met à disposition un fichier par année et par type de collectivité (commune, EPCI, département, région). 
Ils sont téléchargeables via cette [page](http://www.dotations-dgcl.interieur.gouv.fr/consultation/criteres_repartition.php) en choississant l'année et le type de dotation à l'aide des menus déroulants.

## Exploration de la liste des communes

La liste des communes est issue de la première notification annuelle des dotations (fichiers publiés le 15 avril au plus tard).  
On construit un fichier de filiation des communes d'année en année.  
On compare avec des données de mouvement des communes de l'INSEE.
